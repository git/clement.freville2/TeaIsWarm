package fr.uca.iut.clfreville2.teaiswarm.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [CommentEntity::class], version = 1, exportSchema = false)
abstract class TeaDatabase : RoomDatabase() {

    abstract fun commentDao(): CommentDao

    companion object {
        @Volatile
        private var instance: TeaDatabase? = null

        fun getInstance(ctx: Context): TeaDatabase =
            instance ?: synchronized(this) {
                Room.databaseBuilder(
                    ctx.applicationContext,
                    TeaDatabase::class.java,
                    "tea.db"
                ).build().also { instance = it }
            }
    }
}