package fr.uca.iut.clfreville2.teaiswarm.model

import com.squareup.moshi.Json

enum class RepositoryMode {
    @Json(name = "fork")
    FORK,
    @Json(name = "source")
    SOURCE,
    @Json(name = "mirror")
    MIRROR,
    @Json(name = "collaborative")
    COLLABORATIVE,
}