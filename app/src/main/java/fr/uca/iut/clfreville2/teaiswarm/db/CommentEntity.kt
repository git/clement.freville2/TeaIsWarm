package fr.uca.iut.clfreville2.teaiswarm.db

import androidx.room.Entity

@Entity(tableName = "comment", primaryKeys = ["id"])
data class CommentEntity(val id: Int? = null, val owner: String, val repository: String, val comment: String)
