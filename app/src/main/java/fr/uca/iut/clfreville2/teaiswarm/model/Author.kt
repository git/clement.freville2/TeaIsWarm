package fr.uca.iut.clfreville2.teaiswarm.model

import java.util.Date

data class Author(val name: String, val email: String, val date: Date)
