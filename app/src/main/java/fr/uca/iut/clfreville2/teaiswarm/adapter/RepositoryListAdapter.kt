package fr.uca.iut.clfreville2.teaiswarm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.model.Repository

class RepositoryListAdapter(diffCallback: DiffUtil.ItemCallback<Repository>, private val onClick: (Repository) -> Unit) :
    PagingDataAdapter<Repository, RepositoryListAdapter.ViewHolder>(diffCallback) {

    class ViewHolder(view: View, private val onClick: (Repository) -> Unit) : RecyclerView.ViewHolder(view) {
        private val repositoryNameView: TextView
        private val repositoryDescriptionView: TextView
        private val repositoryStarsView: TextView
        private val repositoryLanguageView: TextView
        private val repositoryForksView: TextView
        private var currentRepository: Repository? = null

        init {
            repositoryNameView = view.findViewById(R.id.repo_name)
            repositoryDescriptionView = view.findViewById(R.id.repo_description)
            repositoryStarsView = view.findViewById(R.id.repo_stars)
            repositoryLanguageView = view.findViewById(R.id.repo_language)
            repositoryForksView = view.findViewById(R.id.repo_forks)
            itemView.setOnClickListener {
                currentRepository?.let {
                    onClick(it)
                }
            }
        }

        fun bind(repository: Repository?) {
            if (repository == null) {
                val resources = itemView.resources
                repositoryNameView.text = resources.getString(R.string.loading)
                repositoryDescriptionView.visibility = View.GONE
                repositoryStarsView.text = resources.getString(R.string.unknown)
                repositoryLanguageView.visibility = View.GONE
                repositoryForksView.text = resources.getString(R.string.unknown)
            } else {
                repositoryNameView.text = repository.name

                var descriptionVisibility = View.GONE
                if (repository.description != null) {
                    repositoryDescriptionView.text = repository.description
                    descriptionVisibility = View.VISIBLE
                }
                repositoryDescriptionView.visibility = descriptionVisibility

                repositoryStarsView.text = repository.stars.toString()
                repositoryForksView.text = repository.forks.toString()

                var languageVisibility = View.GONE
                if (!repository.language.isNullOrEmpty()) {
                    val resources = this.itemView.context.resources
                    repositoryLanguageView.text = resources.getString(R.string.language, repository.language)
                    languageVisibility = View.VISIBLE
                }
                repositoryLanguageView.visibility = languageVisibility
            }
            currentRepository = repository
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.repository_view_item, viewGroup, false)

        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    object RepositoryComparator : DiffUtil.ItemCallback<Repository>() {
        override fun areItemsTheSame(oldItem: Repository, newItem: Repository): Boolean =
            oldItem.owner == newItem.owner && oldItem.name == newItem.name

        override fun areContentsTheSame(oldItem: Repository, newItem: Repository): Boolean =
            oldItem == newItem
    }
}
