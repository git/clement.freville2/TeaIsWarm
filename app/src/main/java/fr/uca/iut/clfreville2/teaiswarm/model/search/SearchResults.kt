package fr.uca.iut.clfreville2.teaiswarm.model.search

data class SearchResults<T>(val data: List<T>)
