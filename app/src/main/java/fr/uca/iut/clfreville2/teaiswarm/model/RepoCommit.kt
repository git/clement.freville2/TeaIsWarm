package fr.uca.iut.clfreville2.teaiswarm.model

data class Commit(val author: Author, val committer: Author, val message: String)
