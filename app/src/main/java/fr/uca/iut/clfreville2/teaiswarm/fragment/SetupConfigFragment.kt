package fr.uca.iut.clfreville2.teaiswarm.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.TeaIsWarm
import fr.uca.iut.clfreville2.teaiswarm.USERNAME
import fr.uca.iut.clfreville2.teaiswarm.model.Owner
import kotlinx.coroutines.launch

class SetupConfigFragment(private val preferences: SharedPreferences) : Fragment(R.layout.setup_config) {

    private lateinit var usernameInput: EditText
    private lateinit var confirmButton: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        usernameInput = view.findViewById(R.id.username_input)
        confirmButton = view.findViewById(R.id.configure_button)
        val service = (activity?.application as TeaIsWarm).service
        confirmButton.setOnClickListener {
            val username = usernameInput.text.toString()
            lifecycleScope.launch {
                val owner = service.searchOwner(username)
                if (owner is Owner) {
                    preferences.edit {
                        putString(USERNAME, usernameInput.text.toString())
                    }
                    view.findNavController().navigate(R.id.repository_list_fragment)
                } else {
                    Toast.makeText(
                        view.context,
                        resources.getText(R.string.owner_not_found),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}