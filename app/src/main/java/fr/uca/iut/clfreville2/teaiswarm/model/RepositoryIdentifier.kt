package fr.uca.iut.clfreville2.teaiswarm.model

data class RepositoryIdentifier(val owner: String, val name: String) : RepositoryIdentifiable {

    override val identifier: RepositoryIdentifier
        get() = this
}
