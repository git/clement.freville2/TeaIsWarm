package fr.uca.iut.clfreville2.teaiswarm.network

import fr.uca.iut.clfreville2.teaiswarm.model.*
import fr.uca.iut.clfreville2.teaiswarm.model.search.SearchSettings
import java.util.Date
import kotlin.random.Random

class StubRepositoryService : RepositoryService {

    override suspend fun listActiveRepositories(username: String, page: Int): List<Repository> =
        when (page) {
            1 -> listOf(
                "oki",
                "moshell",
                "scrabble-with-numbers",
                "vdn-tools",
                "codefirst-test",
                "iut-config",
                "Ebullition"
            )
            2 -> listOf(
                "api-ef",
                "codefirst-docdeployer",
                "TeaIsWarm",
                "Application",
                "silex"
            )
            else -> listOf()
        }.map { Repository(Owner(-1, ""), it) }

    override suspend fun listCommits(
        repository: RepositoryIdentifiable,
        sha: String?,
        page: Int
    ): List<CommitActivity> {
        val author = Author("clement.freville2", "clement.freville2@etu.uca.fr", Date())
        return (0..10).map {
            CommitActivity(
                randomCommitSha(),
                Commit(author, author, "Implement parser"),
                null,
                null
            )
        }
    }

    override suspend fun listFileContents(repository: RepositoryIdentifiable, filePath: String) =
        listOf(
            "cli",
            "doc",
            "sql",
            "test",
            "web",
            ".drone.yml",
            ".gitignore",
            "CONVENTIONS.md",
            "README.md"
        ).map { VersionedFile(it, if (it.contains(".")) { FileType.FILE } else { FileType.DIR }) }

    // See KT-2425
    override suspend fun retrieveFileContents(
        repository: RepositoryIdentifiable,
        filePath: String
    ) = FileContent(
        FileType.FILE, 82, """
        #!/bin/bash
        container_id=$(docker run --detach nginx)
        docker stop ${'$'}container_id""", randomCommitSha()
    )

    override suspend fun searchOwner(owner: String): Owner =
        Owner(1, owner)

    override suspend fun searchRepository(repository: RepositoryIdentifiable): Repository? = null

    override suspend fun searchRepositories(settings: SearchSettings): List<Repository> =
        listActiveRepositories(settings.query, settings.page)
}

val CHAR_POOL = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
fun randomCommitSha() = (1..40)
    .map { Random.nextInt(0, CHAR_POOL.size).let { CHAR_POOL[it] } }
    .joinToString("")
