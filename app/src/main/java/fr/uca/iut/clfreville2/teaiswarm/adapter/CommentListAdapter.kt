package fr.uca.iut.clfreville2.teaiswarm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.db.CommentDao
import fr.uca.iut.clfreville2.teaiswarm.db.CommentEntity
import fr.uca.iut.clfreville2.teaiswarm.model.RepositoryIdentifier

class CommentListAdapter : ListAdapter<CommentEntity, CommentListAdapter.CommentViewHolder>(CommentComparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.comment_view_item, parent, false)
        return CommentViewHolder(view)
    }

    override fun onBindViewHolder(holder: CommentViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current.comment)
    }

    class CommentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val commentText: EditText = itemView.findViewById(R.id.comment_edit_text)

        fun bind(text: String?) {
            commentText.setText(text)
        }
    }

    object CommentComparator : DiffUtil.ItemCallback<CommentEntity>() {
        override fun areItemsTheSame(oldItem: CommentEntity, newItem: CommentEntity): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: CommentEntity, newItem: CommentEntity): Boolean {
            return oldItem.comment == newItem.comment
        }
    }

    class CommentViewModel(
        repository: RepositoryIdentifier,
        commentDao: CommentDao
    ) : ViewModel() {
        val allItems: LiveData<List<CommentEntity>> =
            commentDao.getForRepository(repository.owner, repository.name).asLiveData()
    }

    class CommentViewModelFactory(
        private val repository: RepositoryIdentifier,
        private val commentDao: CommentDao
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(CommentViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return CommentViewModel(repository, commentDao) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}