package fr.uca.iut.clfreville2.teaiswarm.model

import com.squareup.moshi.Json

data class Repository(
    val owner: Owner,
    val name: String,
    val description: String? = null,
    @Json(name = "stars_count") val stars: Int = 0,
    @Json(name = "forks_count") val forks: Int = 0,
    val language: String? = null
)
