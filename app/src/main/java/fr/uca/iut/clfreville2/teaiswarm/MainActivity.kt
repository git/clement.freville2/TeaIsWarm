package fr.uca.iut.clfreville2.teaiswarm

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.navigation.fragment.NavHostFragment
import fr.uca.iut.clfreville2.teaiswarm.fragment.RepositoryListFragment
import fr.uca.iut.clfreville2.teaiswarm.fragment.SetupConfigFragment
import fr.uca.iut.clfreville2.teaiswarm.model.Repository
import fr.uca.iut.clfreville2.teaiswarm.model.search.SearchSettings
import fr.uca.iut.clfreville2.teaiswarm.model.search.SortCriteria
import fr.uca.iut.clfreville2.teaiswarm.model.search.SortOrder

const val REPOSITORY_OWNER = "repository_owner"
const val REPOSITORY_NAME = "repository_name"
const val USERNAME = "username"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        val preferences = getPreferences(Context.MODE_PRIVATE)
        supportFragmentManager.fragmentFactory = RepositoryListFragmentFactory(preferences) { repo ->
            adapterOnClick(repo)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preferences.contains(USERNAME) || return
        val nav = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        nav.navController.navigate(R.id.repository_list_fragment)
    }

    private fun adapterOnClick(repository: Repository) {
        val bundle =
            bundleOf(
                REPOSITORY_OWNER to repository.owner.login,
                REPOSITORY_NAME to repository.name
            )
        val nav = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        nav.navController.navigate(R.id.repository_details_fragment, bundle)
    }

    class RepositoryListFragmentFactory(
        private val preferences: SharedPreferences,
        private val onClick: (Repository) -> Unit
    ) : FragmentFactory() {
        override fun instantiate(classLoader: ClassLoader, className: String): Fragment =
            when (className) {
                RepositoryListFragment::class.java.name -> RepositoryListFragment(
                    SearchSettings(sort = SortCriteria.UPDATED, order = SortOrder.DESC), onClick
                )
                SetupConfigFragment::class.java.name -> SetupConfigFragment(preferences)
                else -> super.instantiate(classLoader, className)
            }
    }
}