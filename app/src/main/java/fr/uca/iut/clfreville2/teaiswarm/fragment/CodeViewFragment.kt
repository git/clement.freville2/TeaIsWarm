package fr.uca.iut.clfreville2.teaiswarm.fragment

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.REPOSITORY_NAME
import fr.uca.iut.clfreville2.teaiswarm.REPOSITORY_OWNER
import fr.uca.iut.clfreville2.teaiswarm.TeaIsWarm
import fr.uca.iut.clfreville2.teaiswarm.model.RepositoryIdentifier
import kotlinx.coroutines.launch

class CodeViewFragment : Fragment(R.layout.code_view_fragment) {

    private lateinit var content: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val service = (activity?.application as TeaIsWarm).service
        val bundle = requireArguments()
        val repository = RepositoryIdentifier(
            bundle.getString(REPOSITORY_OWNER)!!,
            bundle.getString(REPOSITORY_NAME)!!
        )
        content = view.findViewById(R.id.code_content_view)
        viewLifecycleOwner.lifecycleScope.launch {
            val contents = service.retrieveFileContents(repository, bundle.getString(FILE_PATH)!!)
            content.text = contents.content
        }
    }
}
