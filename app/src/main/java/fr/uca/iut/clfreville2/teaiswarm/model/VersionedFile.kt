package fr.uca.iut.clfreville2.teaiswarm.model

data class VersionedFile(val name: String, val type: FileType)
