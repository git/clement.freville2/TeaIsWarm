package fr.uca.iut.clfreville2.teaiswarm.network

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import fr.uca.iut.clfreville2.teaiswarm.model.*
import fr.uca.iut.clfreville2.teaiswarm.model.search.SearchResults
import fr.uca.iut.clfreville2.teaiswarm.model.search.SearchSettings

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

const val HTTP_NOT_FOUND = 404

interface GiteaApiService {

    @GET("users/{username}/repos")
    suspend fun listActiveRepositories(@Path("username") username: String, @Query("page") page: Int): List<Repository>

    @GET("repos/{owner}/{repo}/commits")
    suspend fun listCommits(@Path("owner") owner: String, @Path("repo") repo: String, @Query("sha") sha: String?, @Query("page") page: Int): List<CommitActivity>

    @GET("repos/{owner}/{repo}/contents/{filePath}")
    suspend fun listFileContents(@Path("owner") owner: String, @Path("repo") repo: String, @Path("filePath") filePath: String): List<VersionedFile>

    @GET("repos/{owner}/{repo}/contents/{filePath}")
    suspend fun retrieveFileContents(@Path("owner") owner: String, @Path("repo") repo: String, @Path("filePath") filePath: String): FileContent

    @GET("users/{owner}")
    suspend fun searchOwner(@Path("owner") owner: String): Owner?

    @GET("repos/{owner}/{repo}")
    suspend fun searchRepository(@Path("owner") owner: String, @Path("repo") repo: String): Repository?

    @GET("repos/search")
    suspend fun searchRepositories(
        @Query("q") query: String,
        @Query("uid") uid: Int?,
        @Query("team_id") teamId: Int?,
        @Query("starred_by") starredBy: Int?,
        @Query("mode") mode: String?,
        @Query("sort") sort: String,
        @Query("order") order: String,
        @Query("page") page: Int,
        @Query("limit") limit: Int
    ): SearchResults<Repository>
}

class GiteaService(private val handle: GiteaApiService) : RepositoryService {

    constructor() : this(createRetrofit().create(GiteaApiService::class.java))

    override suspend fun listActiveRepositories(username: String, page: Int): List<Repository> = withContext(Dispatchers.IO) {
        if (page < 1) {
            emptyList()
        } else {
            handle.listActiveRepositories(username, page)
        }
    }

    override suspend fun listCommits(
        repository: RepositoryIdentifiable,
        sha: String?,
        page: Int
    ): List<CommitActivity> = withContext(Dispatchers.IO) {
        if (page < 1) {
            emptyList()
        } else {
            handle.listCommits(repository.identifier.owner, repository.identifier.name, sha, page)
        }
    }

    override suspend fun listFileContents(repository: RepositoryIdentifiable, filePath: String): List<VersionedFile> = withContext(Dispatchers.IO) {
        handle.listFileContents(repository.identifier.owner, repository.identifier.name, filePath)
    }

    override suspend fun retrieveFileContents(
        repository: RepositoryIdentifiable,
        filePath: String
    ): FileContent = withContext(Dispatchers.IO) {
        handle.retrieveFileContents(repository.identifier.owner, repository.identifier.name, filePath)
    }

    override suspend fun searchOwner(owner: String): Owner? = withContext(Dispatchers.IO) {
        try {
            handle.searchOwner(owner)
        } catch (ex: HttpException) {
            if (ex.code() == HTTP_NOT_FOUND) {
                null
            } else {
                throw ex
            }
        }
    }

    override suspend fun searchRepository(repository: RepositoryIdentifiable): Repository? = withContext(Dispatchers.IO) {
        try {
            handle.searchRepository(repository.identifier.owner, repository.identifier.name)
        } catch (ex: HttpException) {
            if (ex.code() == HTTP_NOT_FOUND) {
                null
            } else {
                throw ex
            }
        }
    }

    override suspend fun searchRepositories(settings: SearchSettings): List<Repository> = withContext(Dispatchers.IO) {
        if (settings.page < 1) {
            emptyList()
        } else {
            handle.searchRepositories(
                settings.query,
                settings.userId,
                settings.teamId,
                settings.starredBy,
                settings.mode?.toString()?.lowercase(),
                settings.sort.toString().lowercase(),
                settings.order.toString().lowercase(),
                settings.page,
                settings.limit
            ).data
        }
    }
}

private const val CODEFIRST_API_BASE = "https://codefirst.iut.uca.fr/git/api/v1/"

private val httpClient = OkHttpClient()

private fun createRetrofit(): Retrofit =
    Retrofit.Builder()
        .baseUrl(CODEFIRST_API_BASE)
        .addConverterFactory(MoshiConverterFactory.create(
            Moshi.Builder()
                .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
                .add(KotlinJsonAdapterFactory())
                .build()))
        .client(httpClient)
        .build()
