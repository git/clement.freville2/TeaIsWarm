package fr.uca.iut.clfreville2.teaiswarm.network

import fr.uca.iut.clfreville2.teaiswarm.model.*
import fr.uca.iut.clfreville2.teaiswarm.model.search.SearchSettings

interface RepositoryService {

    suspend fun listActiveRepositories(username: String, page: Int): List<Repository>

    suspend fun listCommits(repository: RepositoryIdentifiable, sha: String?, page: Int): List<CommitActivity>

    suspend fun listFileContents(repository: RepositoryIdentifiable, filePath: String): List<VersionedFile>

    suspend fun retrieveFileContents(repository: RepositoryIdentifiable, filePath: String): FileContent

    suspend fun searchOwner(owner: String): Owner?

    suspend fun searchRepository(repository: RepositoryIdentifiable): Repository?

    suspend fun searchRepositories(settings: SearchSettings): List<Repository>
}
