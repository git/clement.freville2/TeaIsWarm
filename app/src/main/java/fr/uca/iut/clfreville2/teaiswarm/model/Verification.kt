package fr.uca.iut.clfreville2.teaiswarm.model

data class Verification(val verified: Boolean)