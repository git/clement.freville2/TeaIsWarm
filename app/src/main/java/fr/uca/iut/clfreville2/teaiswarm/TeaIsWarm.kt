package fr.uca.iut.clfreville2.teaiswarm

import android.app.Application
import fr.uca.iut.clfreville2.teaiswarm.db.TeaDatabase
import fr.uca.iut.clfreville2.teaiswarm.network.GiteaService
import fr.uca.iut.clfreville2.teaiswarm.network.RepositoryService

class TeaIsWarm : Application() {

    val database: TeaDatabase by lazy {
        TeaDatabase.getInstance(this)
    }
    val service: RepositoryService by lazy {
        GiteaService()
    }
}