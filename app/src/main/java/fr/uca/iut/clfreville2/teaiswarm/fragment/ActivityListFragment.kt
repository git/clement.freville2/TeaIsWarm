package fr.uca.iut.clfreville2.teaiswarm.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.paging.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.load
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.REPOSITORY_NAME
import fr.uca.iut.clfreville2.teaiswarm.REPOSITORY_OWNER
import fr.uca.iut.clfreville2.teaiswarm.TeaIsWarm
import fr.uca.iut.clfreville2.teaiswarm.model.CommitActivity
import fr.uca.iut.clfreville2.teaiswarm.model.RepositoryIdentifier
import fr.uca.iut.clfreville2.teaiswarm.network.RepositoryService
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

class ActivityListFragment : Fragment(R.layout.activity_list) {

    private lateinit var service: RepositoryService
    var repository: RepositoryIdentifier? = null
    var sha: String? = null

    private lateinit var pagingAdapter: ActivityAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        service = (activity?.application as TeaIsWarm).service
        if (view is SwipeRefreshLayout) {
            view.setOnRefreshListener {
                updateCommits()
                view.isRefreshing = false
            }
        }
        repository = RepositoryIdentifier(
            requireArguments().getString(REPOSITORY_OWNER)!!,
            requireArguments().getString(REPOSITORY_NAME)!!
        )
        val viewModel by viewModels<ActivityViewModel>(
            factoryProducer = {
                ActivityViewModelFactory(
                    service,
                    repository!!,
                    sha
                )
            }
        )
        pagingAdapter = ActivityAdapter(ActivityComparator)
        val recyclerView = view.findViewById<RecyclerView>(R.id.activity_list_view)
        recyclerView.adapter = pagingAdapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.flow.collectLatest { pagingData ->
                pagingAdapter.submitData(pagingData)
            }
        }
    }

    private fun updateCommits() {
        pagingAdapter.refresh()
    }

    class ActivitySource(
        private val service: RepositoryService,
        private val repository: RepositoryIdentifier,
        private val sha: String?
    ) : PagingSource<Int, CommitActivity>() {

        override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CommitActivity> =
            try {
                val nextPageNumber = params.key ?: 1
                val response = service.listCommits(repository, sha, nextPageNumber)
                LoadResult.Page(
                    data = response,
                    prevKey = nextPageNumber - 1,
                    nextKey = nextPageNumber + 1
                )
            } catch (e: IOException) {
                LoadResult.Error(e)
            } catch (e: HttpException) {
                LoadResult.Error(e)
            }

        override fun getRefreshKey(state: PagingState<Int, CommitActivity>): Int? =
            state.anchorPosition?.let { anchorPosition ->
                val anchorPage = state.closestPageToPosition(anchorPosition)
                anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
            }
    }

    class ActivityAdapter(diffCallback: DiffUtil.ItemCallback<CommitActivity>) :
        PagingDataAdapter<CommitActivity, ViewHolder>(diffCallback) {
        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.activity_row_item, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(getItem(position))
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val commitNameView: TextView
        private val commitAuthorView: TextView
        private val commitAuthorAvatarView: ImageView
        private val commitSha: TextView

        init {
            commitNameView = view.findViewById(R.id.commit_name)
            commitAuthorView = view.findViewById(R.id.commit_author)
            commitAuthorAvatarView = view.findViewById(R.id.commit_author_avatar)
            commitSha = view.findViewById(R.id.commit_sha)
        }

        fun bind(commit: CommitActivity?) {
            commit?.let {
                commitNameView.text = it.commit.message
                commitAuthorView.text = it.commit.author.name
                commitAuthorAvatarView.load(it.author?.avatarUrl)
                commitSha.text = it.sha.substring(0, 8)
            }
        }
    }

    object ActivityComparator : DiffUtil.ItemCallback<CommitActivity>() {
        override fun areItemsTheSame(oldItem: CommitActivity, newItem: CommitActivity): Boolean =
            oldItem.sha == newItem.sha

        override fun areContentsTheSame(oldItem: CommitActivity, newItem: CommitActivity): Boolean =
            oldItem == newItem
    }

    class ActivityViewModel(
        private val service: RepositoryService,
        private val repository: RepositoryIdentifier,
        private val sha: String?
    ) : ViewModel() {
        val flow = Pager(
            PagingConfig(pageSize = 10, enablePlaceholders = true)
        ) {
            ActivitySource(service, repository, sha)
        }.flow
    }

    class ActivityViewModelFactory(
        private val service: RepositoryService,
        private val repository: RepositoryIdentifier,
        private val sha: String?
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ActivityViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return ActivityViewModel(service, repository, sha) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}
