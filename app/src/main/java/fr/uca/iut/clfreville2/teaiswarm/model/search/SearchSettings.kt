package fr.uca.iut.clfreville2.teaiswarm.model.search

import fr.uca.iut.clfreville2.teaiswarm.model.RepositoryMode

data class SearchSettings(
    val query: String = "",
    val userId: Int? = null,
    val teamId: Int? = null,
    val starredBy: Int? = null,
    val mode: RepositoryMode? = null,
    val sort: SortCriteria = SortCriteria.ALPHA,
    val order: SortOrder = SortOrder.ASC,
    val page: Int = 1,
    val limit: Int = 10
)
