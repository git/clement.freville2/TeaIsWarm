package fr.uca.iut.clfreville2.teaiswarm.fragment

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import fr.uca.iut.clfreville2.teaiswarm.*
import fr.uca.iut.clfreville2.teaiswarm.adapter.FileListAdapter
import fr.uca.iut.clfreville2.teaiswarm.model.FileType
import fr.uca.iut.clfreville2.teaiswarm.model.RepositoryIdentifier
import fr.uca.iut.clfreville2.teaiswarm.model.VersionedFile
import fr.uca.iut.clfreville2.teaiswarm.network.RepositoryService
import kotlinx.coroutines.launch
import kotlin.io.path.Path

const val FILE_PATH = "file_path"

class RepositoryDetailsFragment : Fragment(R.layout.repository_details) {

    private lateinit var service: RepositoryService
    private lateinit var repositoryOwner: TextView
    private lateinit var repositoryName: TextView
    private lateinit var repositoryDescription: TextView
    private lateinit var repositoryStars: TextView
    private lateinit var repositoryForks: TextView
    private lateinit var activityButton: Button
    private lateinit var versionedFiles: RecyclerView

    private lateinit var currentRepositoryOwner: String
    private lateinit var currentRepositoryName: String
    private var currentFilePath: String? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        service = (activity?.application as TeaIsWarm).service
        repositoryOwner = view.findViewById(R.id.repository_detail_owner)
        repositoryName = view.findViewById(R.id.repository_detail_name)
        repositoryDescription = view.findViewById(R.id.repository_detail_description)
        repositoryStars = view.findViewById(R.id.repo_stars)
        repositoryForks = view.findViewById(R.id.repo_forks)
        activityButton = view.findViewById(R.id.repository_detail_activity)

        val bundle = requireArguments()
        currentRepositoryOwner = bundle.getString(REPOSITORY_OWNER)!!
        currentRepositoryName = bundle.getString(REPOSITORY_NAME)!!
        currentFilePath = bundle.getString(FILE_PATH)

        repositoryOwner.text = currentRepositoryOwner
        repositoryName.text = currentRepositoryName

        val id = RepositoryIdentifier(currentRepositoryOwner, currentRepositoryName)
        versionedFiles = view.findViewById(R.id.versioned_files_view)
        lifecycleScope.launch {
            val repo = service.searchRepository(id)
            repo?.let {
                repositoryDescription.text = it.description
                repositoryStars.text = it.stars.toString()
                repositoryForks.text = it.forks.toString()
            }

            val repos = service.listFileContents(id, currentFilePath ?: "")
            versionedFiles.adapter = FileListAdapter(repos) {
                    file -> adapterOnClick(file)
            }
        }

        activityButton.setOnClickListener {
            findNavController().navigate(R.id.activity_list_fragment, bundle)
        }

        val tabLayout = view.findViewById<TabLayout>(R.id.tabs)
        resources.getStringArray(R.array.repository_tabs).forEach {
            tabLayout.addTab(tabLayout.newTab().setText(it))
        }

        parentFragmentManager.commit {
            setReorderingAllowed(true)
            add<CommentListFragment>(
                R.id.comment_fragment_container, args = bundleOf(
                    REPOSITORY_OWNER to currentRepositoryOwner,
                    REPOSITORY_NAME to currentRepositoryName
                )
            )
        }
    }

    private fun adapterOnClick(file: VersionedFile) {
        val bundle = bundleOf(
            REPOSITORY_OWNER to currentRepositoryOwner,
            REPOSITORY_NAME to currentRepositoryName,
            FILE_PATH to Path(currentFilePath ?: "", file.name).toString()
        )
        when (file.type) {
            FileType.FILE -> {
                findNavController().navigate(R.id.code_view_fragment, bundle)
            }
            FileType.DIR -> {
                findNavController().navigate(R.id.repository_details_fragment, bundle)
            }
            else -> {}
        }
    }
}