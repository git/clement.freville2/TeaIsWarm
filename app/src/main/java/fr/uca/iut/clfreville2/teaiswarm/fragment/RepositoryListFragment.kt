package fr.uca.iut.clfreville2.teaiswarm.fragment

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.ToggleButton
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.TeaIsWarm
import fr.uca.iut.clfreville2.teaiswarm.adapter.RepositoryListAdapter
import fr.uca.iut.clfreville2.teaiswarm.model.Repository
import fr.uca.iut.clfreville2.teaiswarm.model.search.SearchSettings
import fr.uca.iut.clfreville2.teaiswarm.model.search.SortCriteria
import fr.uca.iut.clfreville2.teaiswarm.model.search.SortOrder
import fr.uca.iut.clfreville2.teaiswarm.network.RepositoryService
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException
import kotlin.properties.Delegates

class RepositoryListFragment(
    private val initialSearch: SearchSettings,
    private val onClick: (Repository) -> Unit
) : Fragment(R.layout.repository_list) {

    private lateinit var service: RepositoryService
    private var lateInit = true
    private var search: SearchSettings by Delegates.observable(initialSearch) { _, _, _ ->
        if (!lateInit) {
            updateRepositories()
        }
    }

    private lateinit var recyclerView: RecyclerView
    private lateinit var pagingAdapter: RepositoryListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (view is SwipeRefreshLayout) {
            view.setOnRefreshListener {
                updateRepositories()
                view.isRefreshing = false
            }
        }
        service = (activity?.application as TeaIsWarm).service
        recyclerView = view.findViewById(R.id.repositories_view)
        val spinner: Spinner = view.findViewById(R.id.sort_by_spinner)
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.sort_criteria,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = SortListener()
        spinner.setSelection(search.sort.ordinal)

        val toggleSort: ToggleButton = view.findViewById(R.id.desc_sort_toggle)
        toggleSort.setOnCheckedChangeListener { _, isChecked ->
            search =
                search.copy(
                    order = if (isChecked) {
                        SortOrder.DESC
                    } else {
                        SortOrder.ASC
                    }
                )
        }
        if (search.order == SortOrder.DESC) {
            toggleSort.isChecked = true
        }

        val viewModel by viewModels<RepositoryViewModel>(
            factoryProducer = {
                RepositoryViewModelFactory(
                    service
                ) { search }
            }
        )
        pagingAdapter =
            RepositoryListAdapter(RepositoryListAdapter.RepositoryComparator, onClick)
        recyclerView.adapter = pagingAdapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.flow.collectLatest { pagingData ->
                pagingAdapter.submitData(pagingData)
            }
        }

        lateInit = true
        search = initialSearch
    }

    private fun updateRepositories() {
        pagingAdapter.refresh()
    }

    class RepositorySource(
        private val service: RepositoryService,
        private val search: SearchSettings
    ) : PagingSource<Int, Repository>() {

        override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Repository> =
            try {
                val nextPageNumber = params.key ?: 1
                val response = service.searchRepositories(search.copy(page = nextPageNumber))
                LoadResult.Page(
                    data = response,
                    prevKey = nextPageNumber - 1,
                    nextKey = nextPageNumber + 1
                )
            } catch (e: IOException) {
                LoadResult.Error(e)
            } catch (e: HttpException) {
                LoadResult.Error(e)
            }

        override fun getRefreshKey(state: PagingState<Int, Repository>): Int? =
            state.anchorPosition?.let { anchorPosition ->
                val anchorPage = state.closestPageToPosition(anchorPosition)
                anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
            }
    }

    class RepositoryViewModel(
        private val service: RepositoryService,
        private val search: () -> SearchSettings
    ) : ViewModel() {
        val flow = Pager(
            PagingConfig(pageSize = 10, enablePlaceholders = true)
        ) {
            RepositorySource(service, search())
        }.flow.cachedIn(viewModelScope)
    }

    class RepositoryViewModelFactory(
        private val service: RepositoryService,
        private val search: () -> SearchSettings
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(RepositoryViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return RepositoryViewModel(service, search) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }

    inner class SortListener : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            search = search.copy(sort = SortCriteria.entries[position])
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {
            search = search.copy(sort = SortCriteria.ALPHA)
        }
    }
}
