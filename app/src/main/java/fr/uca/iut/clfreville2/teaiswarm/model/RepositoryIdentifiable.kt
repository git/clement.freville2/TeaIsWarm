package fr.uca.iut.clfreville2.teaiswarm.model

interface RepositoryIdentifiable {

    val identifier: RepositoryIdentifier
}