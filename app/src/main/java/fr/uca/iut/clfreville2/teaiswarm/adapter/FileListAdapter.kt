package fr.uca.iut.clfreville2.teaiswarm.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.model.VersionedFile

class FileListAdapter(private val dataSet: List<VersionedFile>, private val onClick: (VersionedFile) -> Unit) :
    RecyclerView.Adapter<FileListAdapter.ViewHolder>() {

    class ViewHolder(view: View, private val onClick: (VersionedFile) -> Unit) : RecyclerView.ViewHolder(view) {
        private val fileNameView: TextView
        private var currentFile: VersionedFile? = null

        init {
            fileNameView = view.findViewById(R.id.repository_name)
            itemView.setOnClickListener {
                currentFile?.let {
                    onClick(it)
                }
            }
        }

        fun bind(file: VersionedFile) {
            fileNameView.text = file.name
            currentFile = file
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.repository_row_item, viewGroup, false)

        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    override fun getItemCount() = dataSet.size
}
