package fr.uca.iut.clfreville2.teaiswarm.model

import com.squareup.moshi.Json

enum class FileType {
    @Json(name = "file")
    FILE,
    @Json(name = "dir")
    DIR,
    @Json(name = "symlink")
    SYMLINK,
    @Json(name = "submodule")
    SUBMODULE
}
