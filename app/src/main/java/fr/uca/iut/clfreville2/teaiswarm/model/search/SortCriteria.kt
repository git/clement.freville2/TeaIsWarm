package fr.uca.iut.clfreville2.teaiswarm.model.search

import com.squareup.moshi.Json

enum class SortCriteria {
    @Json(name = "alpha")
    ALPHA,
    @Json(name = "created")
    CREATED,
    @Json(name = "updated")
    UPDATED,
    @Json(name = "size")
    SIZE,
    @Json(name = "id")
    ID
}
