package fr.uca.iut.clfreville2.teaiswarm.model.search

import com.squareup.moshi.Json

enum class SortOrder {
    @Json(name = "asc")
    ASC,
    @Json(name = "desc")
    DESC
}