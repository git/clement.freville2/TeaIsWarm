package fr.uca.iut.clfreville2.teaiswarm.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.uca.iut.clfreville2.teaiswarm.R
import fr.uca.iut.clfreville2.teaiswarm.REPOSITORY_NAME
import fr.uca.iut.clfreville2.teaiswarm.REPOSITORY_OWNER
import fr.uca.iut.clfreville2.teaiswarm.TeaIsWarm
import fr.uca.iut.clfreville2.teaiswarm.adapter.CommentListAdapter
import fr.uca.iut.clfreville2.teaiswarm.db.CommentEntity
import fr.uca.iut.clfreville2.teaiswarm.model.RepositoryIdentifier

class CommentListFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val owner = arguments?.getString(REPOSITORY_OWNER)!!
        val repo = arguments?.getString(REPOSITORY_NAME)!!
        val repository = RepositoryIdentifier(owner, repo)
        val viewModel: CommentListAdapter.CommentViewModel by activityViewModels {
            CommentListAdapter.CommentViewModelFactory(
                repository,
                (activity?.application as TeaIsWarm).database.commentDao()
            )
        }

        (activity?.application as TeaIsWarm).database.commentDao().insert(
            CommentEntity(
                null,
                owner,
                repo,
                "test"
            )
        )

        recyclerView = view.findViewById(R.id.comment_list)

        val adapter = CommentListAdapter()
        recyclerView.adapter = adapter
        viewModel.allItems.observe(this.viewLifecycleOwner) { items ->
            items.let {
                adapter.submitList(it)
            }
        }
        recyclerView.layoutManager = LinearLayoutManager(context)
    }

}