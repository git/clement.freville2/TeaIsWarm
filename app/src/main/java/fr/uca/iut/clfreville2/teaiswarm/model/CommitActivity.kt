package fr.uca.iut.clfreville2.teaiswarm.model

data class CommitActivity(val sha: String, val commit: Commit, val author: Owner?, val committer: Owner?)
