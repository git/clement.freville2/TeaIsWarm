package fr.uca.iut.clfreville2.teaiswarm.db

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface CommentDao {

    @Insert
    fun insert(comment: CommentEntity)

    @Update
    fun update(comment: CommentEntity)

    @Delete
    fun delete(comment: CommentEntity)

    @Query("SELECT * FROM comment WHERE owner = :owner AND repository = :repository")
    fun getForRepository(owner: String, repository: String): Flow<List<CommentEntity>>
}