package fr.uca.iut.clfreville2.teaiswarm.fragment

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import fr.uca.iut.clfreville2.teaiswarm.R

class PreferencesFragment : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) =
        setPreferencesFromResource(R.xml.preferences, rootKey)
}
