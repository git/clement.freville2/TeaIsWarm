package fr.uca.iut.clfreville2.teaiswarm.model

import com.squareup.moshi.Json

data class Owner(val id: Int, val login: String, @Json(name = "avatar_url") val avatarUrl: String = "")
