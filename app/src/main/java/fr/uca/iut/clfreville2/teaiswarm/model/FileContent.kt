package fr.uca.iut.clfreville2.teaiswarm.model

import android.util.Base64
import com.squareup.moshi.Json

data class FileContent(
    val type: FileType,
    val size: Int,
    @Json(name = "content") val contentBase64: String,
    val last_commit_sha: String
) {
    val content: String
        get() = String(Base64.decode(contentBase64, Base64.DEFAULT))
}
